# The workflow Project

## About
A workflow project for Technical Test purpose

You can run this project in Developping or in Production mode by
choosing the right profile.
Production profile is using Mysql.
Developpement profile is using H2 Database
## RESTful Url:
There is two Exposed webservices: 

### Get all workflow categories
"/workflow-categories"

### Get all workflow By Filter and By Page and size
"/workflows?page=0&size=2"
