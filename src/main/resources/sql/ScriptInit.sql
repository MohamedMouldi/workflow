-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour workflow
CREATE DATABASE IF NOT EXISTS `workflow` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `workflow`;

-- Export de la structure de la table workflow. category
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_at` datetime DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_category_id_category` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_category`),
  KEY `FKps2shilwbs1l59our5fa8fu79` (`parent_category_id_category`),
  CONSTRAINT `FKps2shilwbs1l59our5fa8fu79` FOREIGN KEY (`parent_category_id_category`) REFERENCES `category` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Export de données de la table workflow.category : ~2 rows (environ)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` (`id_category`, `create_at`, `enabled`, `update_at`, `description`, `logo`, `name`, `parent_category_id_category`) VALUES
	(1, '2019-10-13 01:26:01', b'1', '2019-10-13 01:26:01', 'category1', 'logo1', 'name1', 3),
	(2, '2019-10-13 01:27:25', b'0', '2019-10-13 01:27:25', 'category2', 'logo2', 'name2', 1),
	(3, '2019-10-13 01:27:25', b'0', '2019-10-13 01:27:25', 'category3', 'logo3', 'name3', NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Export de la structure de la table workflow. category_workflow
CREATE TABLE IF NOT EXISTS `category_workflow` (
  `category_id` bigint(20) NOT NULL,
  `workflow_id` bigint(20) NOT NULL,
  KEY `FKdrhltb7da91shgma531k0x91c` (`workflow_id`),
  KEY `FKd6hl6l66wmg8upb16aw1goqhd` (`category_id`),
  CONSTRAINT `FKd6hl6l66wmg8upb16aw1goqhd` FOREIGN KEY (`category_id`) REFERENCES `category` (`id_category`),
  CONSTRAINT `FKdrhltb7da91shgma531k0x91c` FOREIGN KEY (`workflow_id`) REFERENCES `workflow` (`id_workflow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table workflow.category_workflow : ~6 rows (environ)
/*!40000 ALTER TABLE `category_workflow` DISABLE KEYS */;
INSERT INTO `category_workflow` (`category_id`, `workflow_id`) VALUES
	(1, 1),
	(1, 2),
	(2, 1),
	(2, 2),
	(2, 3),
	(2, 4),
	(3, 1);
/*!40000 ALTER TABLE `category_workflow` ENABLE KEYS */;

-- Export de la structure de la table workflow. parent_category_child_category
CREATE TABLE IF NOT EXISTS `parent_category_child_category` (
  `category_id` bigint(20) NOT NULL,
  `child_category_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_pq2mx8vh5qe205sfo1nsljp83` (`child_category_id`),
  KEY `FKa9e8xgnb8jdxw2k6ggworrv9k` (`category_id`),
  CONSTRAINT `FKa9e8xgnb8jdxw2k6ggworrv9k` FOREIGN KEY (`category_id`) REFERENCES `category` (`id_category`),
  CONSTRAINT `FKigjtf7ns8fh2sixd9sulpb378` FOREIGN KEY (`child_category_id`) REFERENCES `category` (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table workflow.parent_category_child_category : ~0 rows (environ)
/*!40000 ALTER TABLE `parent_category_child_category` DISABLE KEYS */;
INSERT INTO `parent_category_child_category` (`category_id`, `child_category_id`) VALUES
	(1, 2),
	(3, 1);
/*!40000 ALTER TABLE `parent_category_child_category` ENABLE KEYS */;

-- Export de la structure de la table workflow. parent_workflow_child_workflow
CREATE TABLE IF NOT EXISTS `parent_workflow_child_workflow` (
  `id_workflow` bigint(20) NOT NULL,
  `child_workflow` bigint(20) NOT NULL,
  KEY `FKbyep75mddrs9ebl16y59rhq7x` (`child_workflow`),
  KEY `FKof6qftmh19doh2er9x1008bga` (`id_workflow`),
  CONSTRAINT `FKbyep75mddrs9ebl16y59rhq7x` FOREIGN KEY (`child_workflow`) REFERENCES `workflow` (`id_workflow`),
  CONSTRAINT `FKof6qftmh19doh2er9x1008bga` FOREIGN KEY (`id_workflow`) REFERENCES `workflow` (`id_workflow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table workflow.parent_workflow_child_workflow : ~0 rows (environ)
/*!40000 ALTER TABLE `parent_workflow_child_workflow` DISABLE KEYS */;
INSERT INTO `parent_workflow_child_workflow` (`id_workflow`, `child_workflow`) VALUES
	(1, 2),
	(1, 3),
	(2, 4),
	(4, 3);
/*!40000 ALTER TABLE `parent_workflow_child_workflow` ENABLE KEYS */;

-- Export de la structure de la table workflow. workflow
CREATE TABLE IF NOT EXISTS `workflow` (
  `id_workflow` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id_workflow`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Export de données de la table workflow.workflow : ~4 rows (environ)
/*!40000 ALTER TABLE `workflow` DISABLE KEYS */;
INSERT INTO `workflow` (`id_workflow`, `description`, `name`, `status`) VALUES
	(1, 'secondDescription', 'secondWorkflow', 4),
	(2, 'thirdWorkflow', 'thirdWorkflow', 4),
	(3, 'forthWorkflow', 'forthWorkflow', 3),
	(4, 'forthWorkflow', 'fifthWorkflow', 3);
/*!40000 ALTER TABLE `workflow` ENABLE KEYS */;
