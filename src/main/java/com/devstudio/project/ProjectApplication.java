package com.devstudio.project;

import com.devstudio.project.repository.WorkflowCategoryRepository;
import com.devstudio.project.repository.WorkflowRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectApplication implements CommandLineRunner {
    WorkflowRepository workflowRepository;
    WorkflowCategoryRepository workflowCategoryRepository;

    public ProjectApplication(WorkflowRepository workflowRepository, WorkflowCategoryRepository workflowCategoryRepository) {
        this.workflowRepository = workflowRepository;
        this.workflowCategoryRepository = workflowCategoryRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(ProjectApplication.class, args);


    }

    @Override
    public void run(String... args) throws Exception {
////        System.out.println("Test");
//        Workflow workflow = new Workflow();
//        workflow.setName("secondWorkflow");
//        workflow.setDescription("secondDescription");
//        workflow.setStatus(4);
//
//        workflowRepository.save(workflow);
//        Workflow workflow2 = new Workflow();
//        workflow2.setName("thirdWorkflow");
//        workflow2.setDescription("thirdWorkflow");
//        workflow2.setStatus(4);
//
//        workflowRepository.save(workflow2);
//        Workflow workflow3 = new Workflow();
//        workflow3.setName("forthWorkflow");
//        workflow3.setDescription("forthWorkflow");
//        workflow3.setStatus(3);
//
//        workflowRepository.save(workflow3);


//        List<WorkflowCategory> workflowCategoryList = workflowCategoryRepository.findAll();
//        List<Workflow> workflowsDb = workflowRepository.findAll();
////
////        Workflow workflow = workflowRepository.findById(new Long(4)).get();
////        workflow.setWorkflowOf(workflowsDb.subList(0,2));
////        workflowRepository.save(workflow);
//////        Workflow workflow2 = workflowRepository.findById(new Long(6)).get();
//        WorkflowCategory workflowCategory = new WorkflowCategory();
//        workflowCategory.setName("ccc");
//        workflowCategory.setDescription("ddd");
//        workflowCategory.setLogo("eee");
//        workflowCategory.setCreateAt(new Date());
//        workflowCategory.setUpdateAt(new Date());
//        workflowCategory.setEnabled(false);
////        workflowCategory.setParentCategory(workflowCategoryList.get(2));
////        List<Workflow> workflows = new ArrayList<>();
////        workflowCategory.setWorkflowCategoryList(Arrays.asList(workflowCategoryList.get(0),workflowCategoryList.get(1)));
////        workflows.add(workflow);
////        workflows.add(workflow2);
////        workflow.setWorkflowCategoryList(Arrays.asList(workflowCategory));
//        workflowCategory.setWorkflows(workflowsDb);
////
//        workflowCategoryRepository.save(workflowCategory);


    }
}
