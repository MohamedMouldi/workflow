package com.devstudio.project.service.impl;

import com.devstudio.project.model.Workflow;
import com.devstudio.project.model.WorkflowCategory;
import com.devstudio.project.service.utils.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;


/**
 * This class implements the Workflow Specification
 *
 * @author M.Slama
 */

public class WorkflowSpecification implements Specification<Workflow> {

    private SearchCriteria criteria;

    public WorkflowSpecification(final SearchCriteria criteria) {
        super();
        this.criteria = criteria;
    }

    /**
     * This method is used when DB requests are called using this specification.
     *
     * @param root
     * @param criteriaQuery
     * @param criteriaBuilder
     * @return
     */
    @Override
    public Predicate toPredicate(Root<Workflow> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (criteria.getOperation().equalsIgnoreCase("=")) {
            if (criteria.getKey().equals("name")) {
                return criteriaBuilder.equal(root.get("name"), criteria.getValue());
            } else if (criteria.getKey().equals("status")) {
                return criteriaBuilder.equal(root.get("status"), criteria.getValue());
            }
        } else if (criteria.getOperation().equalsIgnoreCase("in")) {
            final Join<Workflow, WorkflowCategory> category = root.join("workflowCategoryList", JoinType.LEFT);
            CriteriaBuilder.In<Object> in = criteriaBuilder.in(category.get("idCategory"));
            for (Object value : (List<?>) criteria.getValue()) {
                if (in != null) {
                    in = in.value(value);
                }
            }
            criteriaQuery.distinct(true);
            return in;
        }

        return null;
    }
}
