package com.devstudio.project.service.impl;

import com.devstudio.project.dto.WorkflowCategoryDto;
import com.devstudio.project.repository.WorkflowCategoryRepository;
import com.devstudio.project.service.WorkflowCategoryService;
import com.devstudio.project.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The WorfklowCateogry Service Implementation class. Handles the WorfklowCateogry Business Logic and
 * access to the Data Layer of the WorfklowCateogry entity
 *
 * @author M.Slama
 */

@Service
public class WorkflowCategoryServiceImpl implements WorkflowCategoryService {

    @Autowired
    WorkflowCategoryRepository workflowCategoryRepository;


    /**
     * This method returns the list of all WorkflowCategoryDto.
     *
     * @return
     */
    @Override
    public List<WorkflowCategoryDto> findAll() {
        return Mapper.convertWorkflowCategoryToWorkflowCategoryDto(workflowCategoryRepository.findAll());
    }
}
