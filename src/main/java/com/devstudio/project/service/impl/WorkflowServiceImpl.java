package com.devstudio.project.service.impl;

import com.devstudio.project.dto.WorkflowFilter;
import com.devstudio.project.model.Workflow;
import com.devstudio.project.repository.WorkflowRepository;
import com.devstudio.project.service.WorkflowService;
import com.devstudio.project.service.utils.SearchCriteria;
import com.devstudio.project.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * The Worfklow Service Implementation class. Handles the Worfklow Business Logic and
 * access to the Data Layer of the Worfklow entity
 *
 * @author M.Slama
 */

@Service
public class WorkflowServiceImpl implements WorkflowService {

    @Autowired
    WorkflowRepository workflowRepository;

    /**
     * Given an object of type WorkflowFilter, this method uses Jpa
     * Specification to filter on the Worfklows in the DB and returns
     * the list of corresponding WorkflowDto
     *
     * @param filter
     * @param pageRequest
     * @return
     */
    @Override
    public Map<String, Object> getWorkflowsByFilter(WorkflowFilter filter, Pageable pageRequest) {
        Map<String, Object> body = new HashMap<>();
        Page<Workflow> workflowPage;
        Specification<Workflow> spec = getSpecWorkflow(filter);
        if (spec != null) {
            workflowPage = workflowRepository.findAll(spec, pageRequest);
        } else {
            workflowPage = workflowRepository.findAll(pageRequest);
        }
        if (workflowPage != null) {
            body.put("result", Mapper.convertWorkflowToWorkflowDto(workflowPage.getContent()));
            body.put("X-Total-Count", workflowPage.getTotalElements());
        }
        return body;
    }

    /**
     * Given an object of type WorkflowFilter, this method return a correspondent specification
     *
     * @param filter
     * @return
     */
    private Specification<Workflow> getSpecWorkflow(WorkflowFilter filter) {
        Specification<Workflow> finalSpec = null;
        if (filter.getName() != null && filter.getName() != "") {
            finalSpec = (finalSpec == null)
                    ? Specification.where(new WorkflowSpecification(
                    new SearchCriteria("name", "=", filter.getName())))
                    : finalSpec.and(new WorkflowSpecification(
                    new SearchCriteria("name", "=", filter.getName())));
        }
        if (filter.getStatus() != null) {
            finalSpec = (finalSpec == null)
                    ? Specification.where(new WorkflowSpecification(
                    new SearchCriteria("status", "=", filter.getStatus())))
                    : finalSpec.and(new WorkflowSpecification(
                    new SearchCriteria("status", "=", filter.getStatus())));
        }
        if (filter.getCategoryIds() != null && !filter.getCategoryIds().isEmpty()) {
            finalSpec = (finalSpec == null)
                    ? Specification.where(new WorkflowSpecification(
                    new SearchCriteria("category", "in", filter.getCategoryIds())))
                    : finalSpec.and(new WorkflowSpecification(
                    new SearchCriteria("category", "in", filter.getCategoryIds())));
        }
        return finalSpec;
    }
}
