package com.devstudio.project.service;

import com.devstudio.project.dto.WorkflowFilter;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * Workflow Service Interface
 *
 * @author M.Slama
 */
public interface WorkflowService {

    Map<String, Object> getWorkflowsByFilter(WorkflowFilter filter, Pageable pageRequest);

}
