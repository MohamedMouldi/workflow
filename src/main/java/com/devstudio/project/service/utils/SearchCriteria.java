package com.devstudio.project.service.utils;

/**
 * This class is a model for criteria used in filters
 *
 * @author M.Slama
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchCriteria {

    /**
     * The filter Key
     */
    private String key;

    /**
     * The filter Operation
     */
    private String operation;

    /**
     * The filter Value
     */
    private Object value;

}