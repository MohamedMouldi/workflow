package com.devstudio.project.service;

import com.devstudio.project.dto.WorkflowCategoryDto;

import java.util.List;

/**
 * WorkflowCategory Service Interface
 *
 * @author M.Slama
 */

public interface WorkflowCategoryService {

    List<WorkflowCategoryDto> findAll();

}
