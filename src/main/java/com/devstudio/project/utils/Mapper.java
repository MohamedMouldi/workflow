package com.devstudio.project.utils;

import com.devstudio.project.dto.WorkflowCategoryDto;
import com.devstudio.project.dto.WorkflowDto;
import com.devstudio.project.model.Workflow;
import com.devstudio.project.model.WorkflowCategory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides the DTO Mapping methods
 *
 * @author M.Slama
 */
public class Mapper {
    public static List<WorkflowCategoryDto> convertWorkflowCategoryToWorkflowCategoryDto(List<WorkflowCategory> workflowCategoryList) {
        List<WorkflowCategoryDto> workflowCategoryDtoList = new ArrayList<>();
        for (WorkflowCategory workflowCategory : workflowCategoryList) {
            workflowCategoryDtoList.add(
                    new WorkflowCategoryDto(workflowCategory.getIdCategory(), workflowCategory.getName(), workflowCategory.getDescription(),
                            workflowCategory.getLogo(), workflowCategory.getCreateAt(), workflowCategory.getUpdateAt(),
                            workflowCategory.getEnabled())
            );
        }
        return workflowCategoryDtoList;
    }

    public static List<WorkflowDto> convertWorkflowToWorkflowDto(List<Workflow> workflowList) {
        List<WorkflowDto> workflowDtoList = new ArrayList<>();
        for (Workflow workflow : workflowList) {
            workflowDtoList.add(new WorkflowDto(workflow.getName(), workflow.getDescription(), workflow.getStatus()));
        }
        return workflowDtoList;
    }
}
