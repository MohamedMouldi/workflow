package com.devstudio.project.repository;

import com.devstudio.project.model.WorkflowCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data access layer/repository for the WorkflowCategory entity
 *
 * @author M.Slama
 */

@Repository
public interface WorkflowCategoryRepository extends JpaRepository<WorkflowCategory, Long> {
}
