package com.devstudio.project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

/**
 * The Workflow entity. Represents a Workflow and its fields which are persisted in the DB
 *
 * @author M.Slama
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Workflow {
    /**
     * Entity Auto Generated Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idWorkflow;

    /**
     * Workflow Name
     */
    private String name;


    /**
     * Workflow Description
     */
    private String description;

    /**
     * Workflow status. Should be an integer between 1 and 5
     */
    @Min(value = 1)
    @Max(value = 5)
    private Integer status;

    /**
     * List of child workflows
     */
    @ManyToMany
    @JoinTable(name = "parent_workflow_child_workflow", joinColumns = @JoinColumn(name = "idWorkflow"),
            inverseJoinColumns = @JoinColumn(name = "childWorkflow"))
    private List<Workflow> workflows;

    /**
     * List of parent workflows
     */
    @ManyToMany
    @JoinTable(name = "parent_workflow_child_workflow", joinColumns = @JoinColumn(name = "childWorkflow"),
            inverseJoinColumns = @JoinColumn(name = "idWorkflow"))
    private List<Workflow> workflowOf;

    /**
     * List of workflow categories
     */
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "workflows")
    private List<WorkflowCategory> workflowCategoryList;


}
