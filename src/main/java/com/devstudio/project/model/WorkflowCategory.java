package com.devstudio.project.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * The WorkflowCategory entity. Represents a Workflow Category and its fields which are persisted in the DB
 *
 * @author M.Slama
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Category")
public class WorkflowCategory {

    /**
     * Entity Auto Generated Id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCategory;

    /**
     * Workflow Category name
     */
    private String name;

    /**
     * Workflow Category description
     */
    private String description;

    /**
     * Workflow Category logo
     */
    private String logo;

    /**
     * Workflow Category creation date
     */
    private Date CreateAt;

    /**
     * Workflow Category update date
     */
    private Date UpdateAt;

    /**
     * Field for Workflow Category status : Enabled or Disabled ( true or false )
     */
    private Boolean Enabled;

    /**
     * The parent Worfklow Category of this Object
     */
    @OneToOne(fetch = FetchType.LAZY)
    private WorkflowCategory parentCategory;

    /**
     * List of Workflow Categories in relation with this Object
     */
    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "parent_category_child_category",
            joinColumns = {@JoinColumn(name = "category_id")},
            inverseJoinColumns = {@JoinColumn(name = "child_category_id")})
    private List<WorkflowCategory> workflowCategoryList;

    /**
     * List of Workflow related to this Object
     * This list should not be empty
     */
    @NotNull
    @Size(min = 1)
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "category_workflow",
            joinColumns = {@JoinColumn(name = "category_id")},
            inverseJoinColumns = {@JoinColumn(name = "workflow_id")}
    )
    private List<Workflow> workflows;

}
