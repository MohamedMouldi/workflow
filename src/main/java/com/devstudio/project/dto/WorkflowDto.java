package com.devstudio.project.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Data Transfer Object for the Workflow entity. Mapping function at class
 * utils.Mapper
 *
 * @author M.Slama
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkflowDto {
    private String name;
    private String description;
    private Integer status;
}
