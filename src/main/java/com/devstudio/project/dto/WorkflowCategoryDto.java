package com.devstudio.project.dto;

import com.devstudio.project.model.WorkflowCategory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Data Transfer Object for the WorkflowCategory entity. Mapping function at class
 * utils.Mapper
 *
 * @author M.Slama
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WorkflowCategoryDto {
    private Long idCategory;
    private String name;
    private String description;
    private String logo;
    private Date CreateAt;
    private Date UpdateAt;
    private Boolean Enabled;
}
