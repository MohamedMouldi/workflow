package com.devstudio.project.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

/**
 * Represents the possible filters/fields against which we get the desired list of Workflows
 *
 * @author M.Slama
 */

@Data
public class WorkflowFilter {
    private String name;
    private List<Long> categoryIds;
    @Min(value = 1)
    @Max(value = 5)
    private Integer status;
}
