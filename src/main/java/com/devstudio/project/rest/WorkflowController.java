package com.devstudio.project.rest;

import com.devstudio.project.dto.WorkflowFilter;
import com.devstudio.project.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * Represents the RESTful controller layer for the Workflow services
 *
 * @author M.Slama
 */

@RestController
@RequestMapping("/workflows")
public class WorkflowController {

    @Autowired
    WorkflowService workflowService;

    /**
     * Given a Filter object containing filtering fields(name, status, idCategories),
     * this method returns the filtered list of worfklows.
     *
     * @param page
     * @param size
     * @param workflowFilter
     * @return
     */
    @PostMapping()
    public ResponseEntity<Map<String, Object>> getWorkflowsByFilter(@RequestParam("page") int page,
                                                                    @RequestParam("size") int size,
                                                                    @RequestBody @Valid WorkflowFilter workflowFilter) {
        try {
            Map<String, Object> filtredWorkflows = workflowService.getWorkflowsByFilter(workflowFilter, PageRequest.of(page, size));
            return new ResponseEntity<>(filtredWorkflows, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
