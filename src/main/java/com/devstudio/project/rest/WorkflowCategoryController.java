package com.devstudio.project.rest;

import com.devstudio.project.dto.WorkflowCategoryDto;
import com.devstudio.project.service.WorkflowCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Represents the RESTful controller layer for the WorkflowCategory services
 *
 * @author M.Slama
 */


@RestController
@RequestMapping("/workflow-categories")
public class WorkflowCategoryController {

    @Autowired
    WorkflowCategoryService workflowCategoryService;

    /**
     * This method returns the list of all Workflow Categories.
     *
     * @return
     */
    @GetMapping()
    public ResponseEntity<List<WorkflowCategoryDto>> getAll() {
        return new ResponseEntity<>(workflowCategoryService.findAll(), HttpStatus.OK);
    }
}
