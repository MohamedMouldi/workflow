package com.devstudio.project;

import com.devstudio.project.dto.WorkflowDto;
import com.devstudio.project.dto.WorkflowFilter;
import com.devstudio.project.service.WorkflowService;
import org.h2.tools.Server;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * This class is a Test Class for Workflow Service
 *
 * @author M.Slama
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql("classpath:WorfklowServiceTest.sql")
public class WorfklowServiceTest {

    @Autowired
    WorkflowService workflowService;

    WorkflowFilter workflowFilter = new WorkflowFilter();

    @Before
    public void beforeTest() throws SQLException {
        Server webServer = Server.createWebServer("-web",
                "-webAllowOthers", "-webPort", "8082");
        webServer.start();
    }

    @Test
    public void getWorkflowsByFilter_Filter_ReturnFiltredList() {
        workflowFilter.setName("secondWorkflow");
        Map<String, Object> result = workflowService.getWorkflowsByFilter(workflowFilter, PageRequest.of(0,50));
        Assert.assertEquals(new Long(1), result.get("X-Total-Count"));
        Assert.assertEquals("secondWorkflow", ( (List< WorkflowDto >) result.get("result")).get(0).getName());
        workflowFilter = new WorkflowFilter();
        workflowFilter.setCategoryIds(Arrays.asList(new Long(2)));
        Map<String, Object> result2 = workflowService.getWorkflowsByFilter(workflowFilter, PageRequest.of(0,50));
        Assert.assertEquals(new Long(4), result2.get("X-Total-Count"));
    }

}
