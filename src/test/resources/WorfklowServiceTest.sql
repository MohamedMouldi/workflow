INSERT INTO category (id_category, create_at, enabled, update_at, description, logo, name, parent_category_id_category)
VALUES (1, '2019-10-13 01:26:01', TRUE, '2019-10-13 01:26:01', 'aa', 'bb', 'cc', NULL),
       (2, '2019-10-13 01:27:25', FALSE, '2019-10-13 01:27:25', 'dd', 'ee', 'ff', NULL);


INSERT INTO workflow (id_workflow, description, name, status)
VALUES (1, 'secondDescription', 'secondWorkflow', 4),
       (2, 'thirdWorkflow', 'thirdWorkflow', 4),
       (3, 'forthWorkflow', 'forthWorkflow', 3),
       (4, 'forthWorkflow', 'fifthWorkflow', 3);


INSERT INTO category_workflow (category_id, workflow_id)
VALUES (1, 1),
       (1, 2),
       (2, 1),
       (2, 2),
       (2, 3),
       (2, 4);